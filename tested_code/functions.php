<?php


/*
 * Return the sum of two numbers
 * 
 * @param integer $a
 * @param integer $b
 * 
 * @return integer The sum of two numbers
 */

function add($a, $b){
    return $a + $b;
}

