<?php

use PHPUnit\Framework\TestCase;

class FunctionsTest extends TestCase
{
    public function testTwoPlusTwoEqualsFour(){
        require 'tested_code/functions.php';
        
        $this->assertEquals(4, add(2,2));
        $this->assertEquals(8, add(3,5));
    }
    
    public function testAddDoesNotReturnTheIncorrectSum(){
        $this->assertNotEquals(5, add(2,2)); 
        
    }
    
    
}
